FROM nginx:alpine
COPY index.html /usr/share/nginx/html/index.html
COPY index.jpg /usr/share/nginx/html/index.jpg
CMD ["nginx", "-g", "daemon off;"]